# 20240122imprimer les troubles

## Conception

Cette édition papier a été initiée dans le cadre d’un workshop intitulé «Imprimer les troubles», à l'École de Recherche Graphique (erg[^1]) à Bruxelles, du 23 au 25 janvier 2024 et poursuivie par les étudiant·es lors des cours de design numérique. L'objectif du workshop était de concevoir collectivement une version imprimée des N°1 et N°3 de la revue en ligne «Troubles dans les collections»[^2], à l’aide de languages de programmation web. 

L’édition est conçue avec l’outil Ether2html[^3], qui permet de mettre en forme à plusieurs mains du contenu écrit en language markdown via Etherpad, un éditeur de texte en ligne. Le contenu récupéré depuis le site de la revue en ligne est converti en HTML et stylé en CSS. Le tout est visualisé à l’aide de la librairie javascript Paged.js qui offre une interface permettant de travailler la mise en page d’un livre au sein d’un navigateur web, avant d’en générer un pdf imprimable. Enfin, un script JavaScript est utilisé pour l'imposition des pages en livret. Pour travailler ensemble et partager nos modifications, nous avons eu recours au logiciel Gitlab[^4]. 

Nous nous sommes accordé·es sur l’usage principal de la typographie Minipax, dessinée par Raphaël Ronot et diffusée sous license SIL Open Font License, inspirée du roman *1984* de George Orwell dont une oblique a été générée pour l’occasion, la Minitrouble.

Parmi les autres typographies présentes distribuées sous licenses libres : 
- *DINdong* de Clara Sambot
- *EBGaramond* de Georg Duffner et Octavio Pardo 
- *Garamon(d/t)* de fonderie.download
- *Syne Mono* et *Syne Tactile* de Bonjour Monde
- *Toren* et *Epistle* de Eli Heuer

## Contributeurices

- Mondher Aounallah
- Lotte Arndt
- Victoria Bayon de Noyer
- Sam Boibineuf
- Alexia de Visscher
- Delyo Dobrev
- Titouan Dresse
- Amélie Dumont
- Antoine Gelgon
- Louka Langenbick
- Zeste Le Reste
- Martin Lemaire
- Lionel Maes
- Lou  Meessen
- Alice Néron 

[^1]:https://erg.be
[^2]: https://troublesdanslescollections.fr/
[^3]:https://osp.kitchen/tools/ether2html/
[^4]:https://gitlab.com/erg-design-numerique/workshops/202401imprimer-les-troubles
