[Poème de Thierno Seydou Sall, 2018](sound/09/plaidoyerthierno.wav)
###### Prise de son, Moussa Diene

Nos mythes, nos usages, nos légendes, nos contes sont notre passeport culturel pour nous rendre dans les aéroports des pays du reste de l’humanité.

Nos identités culturelles sont notre groupe sanguin en cas d’accident ou de perte de sens liée à une hémorragie grave…

L’ IFAN est un institut où vivent des toiles d’araignées.

Nos chirurgiens, qui sont nos artistes, nous offrent des perfusions…

Les Africains doivent s’investir dans cet institut afin de ne pas disparaître dans les cataclysmes des pertes culturelles, au cœur des tsunamis du monde numérique globalisé.(…)

![](img/09/01.jpg)
###### Bukut, Casamance, 1982 © Jacques Chérel