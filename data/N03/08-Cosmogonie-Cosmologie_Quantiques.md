![](img/08/01.jpg)
###### Josèfa Ntjam, Hilolombi, vidéo, 2018

Lorsque les nouvelles technologies ont permis à leurs utilisateur.rice.s de téléverser des millions

de vidéos, sons, images dans le cyberespace et le web,

alors l’internet commença à être perçu

comme une partie intégrante du monde réel.

Cette vaste opération de traduction du monde par le langage informatique,

de toute information visuelle et sonore, concéda, à ses débuts,

aux corps sans identités,

aux corps fluides,

aux objets sans nom, de circuler librement.

Internet se fit le vaste sismographe de nos gestes, mouvements, mots, pensées, sons, artefacts, calculant, recoupant et disséminant nos données

dans ce paysage infini où les temps éclatés se propagent.

Un paysage figé dans une forme d’ultra-présent troublant, un espace-temps ?

Internet nous fit la promesse d’un modèle nouveau, composé de relations rhizomiques et horizontales

La déterritorialisation pour se libérer des frontières…

Petit à petit, se dénudant de ces qualités d’imperfections,

il sédimenta,

et à sa surface apparut le visage familierd’un système de neutralisation, d’hégémonie et d’invisibilisation des cultures.

*« Vous ne me ressemblez pas. »*

Josèfa Ntjam reconnaît ce visage dans son écran qui ne lui ressemble pas.

Elle explore les espaces-temps de (ses) Internet(s) cherchant à déconstruire les discours hégémoniques sur les notions d’origine, d’identité et d’Histoire.

Sa voix vient réécrire par-dessus, tel un palimpseste.

Elle interroge les méthodes de production de l’histoire dans les recoins des pages internet. Bloquée devant son écran, telle une sans-barque incapable de traverser la rive, elle étudie le mouvement des autres « déliant les mots de son écran ».

Sa voix se balance

avec et contre

les flots rythmés par une mélodie cyclique.

Le mouvement de sa voix face à l’inertie de la réalité.

A l’échelle quantique, les objets sont en constants mouvements, c’est un principe bien connu. Ce principe fondamental de vibration est central dans les cosmologies bantoues.

La physique quantique confirma ce que les philosophies spirituelles bantoues avaient déjà supposé : Tout autour de nous est énergie et nous le sommes aussi.

Les malinkes disent FOLI*, toutes les choses sont du rythme et il n’y a pas de mouvements sans rythmes.

Cette vibration, c’est aussi le principe d’un texte déclamé, d’un texte prévu pour la diction, déversé par le rythme de la parole.

Par la voix, Josèfa devient griot.te, éveilleur.euse de conscience, elle parle, en chair-mots-de-passe**, une écriture incarnée pensée pour sortir de l’éditeur texte de son écran.

Par la voix, elle déconstruit le temps,

ce temps linéaire qui n’existe pas.

Du point de vue de la physique quantique, le temps n’existe pas.

Son écoulement est une illusion.

Josèfa Ntjam le compte en barre mediaplayer.

Ce temps élastique est déformé en permanence, sans début, sans fin.

Elle peut y naviguer dans toutes les directions.

Refuser le temps linéaire, c’est refuser l’impérialisme de ce visage dans l’écran, c’est penser autrement sa relation et celle des objets à cet espace.

Gravitation, superposition, atomisation, dispersion de particules, flux, indétermination…

Joséfa Ntjam prend les attributs d’un objet quantique, insaisissable

puisque potentiellement à plusieurs endroits à la fois.

Indéterminable,

s’échappant au moment de la nomination.

Cette position s’aligne avec les cosmogonies de ses ancêtres,

« les seuls dont elle aime parler désormais ».

Il est accepté aujourd’hui grâce aux travaux de philosophes, tels ceux de John Mbiti, de considérer que la question du temps est élaborée d’une façon singulière par les cosmologies et sociétés africaines et que celle-ci se rapproche des questions explorées par la mécanique quantique.

Josèfa Ntjam rejoint ses ancêtres dans le trouble.

*Masques et avatars*

*Quantum Mecanic* nous parle des internets à l’ère où les avatars remplaçaient encore nos identités.

« Brouiller les pistes ».

Nos doubles cryptés comme des masques pour des rituels numériques, nos êtres dissous derrière des masques-passeports.

Des calques de flous pour nos données empêchant les systèmes de nomination d’identités fixes d’opérer. Fuir la désignation,

rester entre deux mondes

des manières de persister dans le trouble.

Le masque-avatar permet d’habiter ce trouble, il ne dessinera toujours que le tracé basse résolution de l’utilisateur.rice qui s’y cache.

L’écart

entre le masque virtuel et l’utilisateur.rice est volontaire, quand ille le porte son visage s’y dissout.

Avant le web 2.0, les ewusus, sorciers camerounais bassa maîtrisaient déjà la dématérialisation des corps. Capables de séparer leur esprit de leur corps, ils abandonnent leur enveloppe charnelle à la nuit tombée.

Invisibles pour les autres,

ils agissent impunément,

seuls ou en groupe,

liés par un pacte de silence les empêchant de révéler les actes de ces avatars ancestraux nocturnes.

Chiffrage, cryptage, basse résolution,

tels les rituels que suivent les ewusus pour se détacher de leurs corps,

les stratégies de discrétion employées en ligne permettent de demeurer dans l’impalpable.



[Quantum Mecanic, Josèfa Ntjam](https://soundcloud.com/josefa-2/quantum-mecanic-montage)
###### Quantum Mecanic, Josèfa Ntjam, 2020

[Lien Cassé]()
###### Watery Thoughts, Josèfa Ntjam, 2020