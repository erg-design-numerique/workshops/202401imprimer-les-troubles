## « J’utilise la photographie pour rendre la vitalité et la force de leur épopée, la mettre en scène »

**Giulia Paoletti** – Cet été, Ibrahima, tu as effectué une résidence dans les photothèques de l’Institut Fondamental d’Afrique Noire/UCAD et du musée Théodore Monod d’art africain afin de concevoir un projet photographique. Ces photothèques contiennent un patrimoine visuel important sur l’Afrique de l’Ouest. Comment as-tu choisi ton sujet de recherche ? Quel a été ton processus de travail ?

**Ibrahima Thiam** – J’ai été invité en tant que photographe dans le cadre du projet de recherche *Ateliers de Troubles épistémologiques* pour une résidence de création de 3 mois au centre de documentation du musée de Théodore Monod d’Art africain et au service visuel de l’université Cheikh Anta Diop. L’idée était de reconsidérer les archives photographiques qui, elles aussi, sont des collections. J’explore la photographie comme un support de narration, d’adaptation, de recréation, de représentation. Mon travail s’intéresse à la mémoire, l’archive, l’oralité, aux histoires, aux sociétés et aux mythologies africaines.

En visitant ces deux institutions, j’ai découvert leurs photothèques, et j’ai constaté qu’il y a beaucoup de choses à regarder, à lire, à voir et à toucher. Elles conservent un très grand nombre de photographies classées dans des armoires en bois et en métal, mais aussi des ouvrages, des revues, des documents visuels, écrits, et sonores.

Dans ces collections, il y a énormément d’archives classées sous les rubriques – anthropologie, art, société, botanique, médecine, préhistoire, religion, etc – concernant de nombreux pays africains subsahariens. Et notamment des photographies des pratiques animistes de ces pays, comme le vodoun au Bénin par exemple. Mais également des images montrant d’autres pratiques animistes du Sénégal, celles des Bassaris, des Diolas,…

Mon choix s’est porté dès le début sur l’histoire de l’animisme au Sénégal, et j’ai décidé de chercher des éléments liés à la communauté Lébou et à ses réponses sociétales. J’ai trouvé des images exceptionnelles de cette culture animiste fondée sur l’initiation et le secret, telles par exemple des images de Ndeup. Le Ndeup est un culte animiste, un rituel, ciment d’une société dans laquelle certaines croyances et pratiques relient fortement les hommes, la nature et le cosmos. J’ai toujours considéré la photographie comme un outil qui offre la possibilité de transcender les limites physiques et matérielles des archives. Un instrument pour connaitre le monde des autres. Je ne suis pas lébou, mais à Saint-Louis, ma ville natale, la communauté Lébou est très importante.

![](img/04/01.jpg)
###### Ibrahima Thiam, Dem ak tey, 2021.

**Giulia Paoletti** – Peux-tu expliquer le travail que tu as mené sur les Lébou, car tu t’es déjà intéressé au rite de Ndeup avant ce projet, notamment dans ton exposition en 2020, *Ibrahima Thiam : d’une rive à l’autre* ?

**Ibrahima Thiam** – J’ai commencé à m’intéresser aux Lébou en 2017, j’ai lu le livre *N’döep, transe thérapeutique chez les Lébous du Sénégal* (2010) du professeur Omar Ndoye qui est enseignant-chercheur à l’Institut de recherches et d’Enseignement de psychopathologie (Université Cheikh Anta Diop) il était aussi au service de psychiatrie du CHU Fann. Il faut savoir que ce rituel a connu une certaine notoriété due à l’intérêt que lui a été porté l’hôpital de Fann à Dakar et le psychiatre Henri Colomb dans les années 1960.

Ces rites permettent à la communauté de se ressouder et de trouver une cohésion sociale. Ce syncrétisme entre religion et sacré, animisme et humanisme apporte un vivifiant souffle de paix, d’espoir et de sagesse. C’est une des principales cérémonies religieuses traditionnelles qui ponctuent la vie de la collectivité Lébou à Rufisque, Mbao, Ouakam, Bargny, Ngor, Yoff. Aujourd’hui il est important de réhabiliter ces savoirs endogènes méconnus et négligés, qui sont toujours vivants, mais qui connaissent aussi des transformations importantes, sur la presqu’ile du Cap vert.

Dans l’exposition à Raw Material, j’ai revisité à travers des récits oraux et par la photographie l’histoire des génies invisibles protecteurs et protectrices de la communauté Lébou. Cette communauté est concentrée au Sénégal sur la presqu’ile du Cap-Vert. Elle est majoritairement musulmane mais a conservé des pratiques issues de sa religion ancienne. Mes imaginaires sont imprégnés par les légendes sur les divinités Lébou et leurs relations à la nature. L’eau et les rituels y afférant constituent des pratiques sacrées effectuées pour maintenir de bonnes relations avec les esprits protecteurs de ces villes et leurs communautés. La série photographique mettait en présence des imaginaires entre les rives des mondes visibles et invisibles, entre terre et eau. Par exemple, j’ai convoqué dans mes photographie Maam Njaré ou Maam Kumba Bang, des divinités de la mer. Une autre photographie montrait un baobab dans une des cours (*penc*) du plateau, sur lequel j’avais fixé une peau d’animal et posé à son pied des canaris comme dans un autel (*xamb*) de Ndeup. J’utilise la photographie pour rendre la vitalité et la force de leur épopée, la mettre en scène.

![](img/04/02.jpg)
###### Ibrahima Thiam, Installation dans le parc du Musée T. Monod, avec des reproductions de photographies d’un auteur anonyme datant de 1956, à Rufisque, de la collection de l’IFAN Cheikh Anta Diop, août 2021.

**Giulia Paoletti** – Les photographies de Ndeup que tu as trouvées au musée, étaient-elles identiques à celles que tu as vu dans des livres ?

**Ibrahima Thiam** – Les photographies sont différentes. J’ai trouvé des photographies de la communauté Lébou – conservées en sauvegarde numérique – classées dans la rubrique « animisme » – parmi d’autres photos qui sont soigneusement stockées. Elles montrent des scènes du rituel, les gestes, les corps, les instruments de musique, les guérisseuses, les lieux, les habitats, les arbres, la végétation. Ce que j’ai aimé, c’était qu’elles montrent les femmes, car se sont elles qui organisaient le Ndeup. Même le prêtre ndeupkat El Hadj Daouada Seck, à Bargny, était habillé en femme, car les rôles principaux dans ces rituels sont détenus par les femmes. Dans la communauté Lébou, beaucoup d’hommes sont des musulmans, mais les femmes sont les gardiennes de cette culture animiste.

Le Ndeup est aussi fait de la musique, des danses, des transes, des invocations. Et à travers ces photographies, tu vois la danse associée aux invocations qui permettent de communiquer avec les forces surnaturelles. Et puis, dans ces images, on voit les rencontres avec la science des plantes, les baobabs, cela m’a conduit à l’espace du parc du musée et aux arbres qui s’y trouvent, notamment à un jeune baobab. Tout cela m’a parlé.

En fréquentant ces institutions, on apprend beaucoup de choses sur nos coutumes. Ces institutions sont des vraies mines d’or et l’on peut y trouver plein d’informations, par exemples des modèles de coiffure, des scènes de vie, d’artisanat, des objets, des photos des voyages scientifiques, mais aussi des journaux, revues, catalogue de collection, ouvrages illustrés etc. … Il est urgent de questionner et de reconstituer par l’art, la mode, la photographie, le cinéma, la sculpture la compréhension de notre histoire et donc de notre avenir. Il faut nous nourrir de notre histoire et de notre passé pour pouvoir les revisiter et en faire une relecture.

**Giulia Paoletti** – As-tu cherché l’origine et l’auteur des photographies archivées ?

**Ibrahima Thiam** – Les photographies ont été prises en 1956 à Rufisque et à Mbott au plateau de Dakar. Il n’avait pas les noms des photographes; elles sont anonymes. Elles sont conservées au service audiovisuel de l’université.

**Giulia Paoletti** – La pratique du Ndeup a des qualités thérapeutiques, elle a une action psychologique. Pourrais-tu en dire plus ?

Conformément à la tradition spirituelle, certaines maladies mentales continuent d’être traitées chez les Lébou par des cérémonies rituelles de guérison. Le Ndeup est effectué par et pour la communauté lorsque l’un de ses membres manifeste certains troubles du comportement qui sont attribués à des forces surnaturelles. Ces troubles sont le fait d’un génie communément appelé « Rab », qui cherche à instaurer une alliance avec la personne qu’il a investie.

Le but du Ndeup est de réintégrer l’individu dans sa famille. L’esprit sera nommé, honoré et fixé dans l’autel domestique pour se soumettre à sa volonté, qui n’est autre que la volonté des ancêtres. Par ces actes, on reconnait la place que cet individu occupe dans l’ensemble familial, cela permet de resserrer les liens avec la grande communauté des morts et des vivants.

![](img/04/03.jpg)
###### Auteur anonyme. Photographie datant de 1956, à Rufisque, collection de l'IFAN Cheikh Anta Diop.

**Giulia Paoletti** – Est-ce la dimension thérapeutique du Ndeup qui t’intéresse ou le rituel en général ?

**Ibrahima Thiam** – Tous ces savoirs endogènes m’intéressent, ce sont des sciences humaines. L’écrivain lébou Birago Diop a écrit dans son poème *Souffle* en 1960 : « Ecoute plus souvent les choses que les êtres, les morts ne sont pas morts », Écoute plus souvent. Les Choses que les Êtres. La Voix du Feu s’entend, Entends la Voix de l’Eau. Écoute dans le Vent. Le Buisson en sanglots : C’est le Souffle des ancêtres. Ceux qui sont morts ne sont jamais partis : Ils sont dans l’Ombre qui s’éclaire. Et dans l’ombre qui s’épaissit. Les Morts ne sont pas sous la Terre : Ils sont dans l’Arbre qui frémit, Ils sont dans le Bois qui gémit, Ils sont dans l’Eau qui coule, Ils sont dans l’Eau qui dort, Ils sont dans la Case, ils sont dans la Foule : Les Morts ne sont pas morts ».

El Hadj Daouda Seck, grand prêtre du Ndeup a dit que « les Djinnés (les esprits) l’ont choisi « et que ses connaissances lui ont été transmises par «  sa mère et sa grand-mère », il était guérisseur et psychiatre à la fois. C’était un des plus grands guérisseurs Lébou des maladies mentales. Il prenait soin des morts et des vivants. Paix à son âme.

Chez les Lébou, il y a toujours cette idée que les morts sont présents au milieu des vivants. L’idée de réciprocité est importante, l’ensemble du vivant prend soin de l’ensemble du vivant.

Durant ma résidence au musée, je suis allé à la recherche du Ndeup, je suis allé regarder et filmer un Ndeup avec la communauté Lébou dans le quartier de Gueule Tapée à Dakar. Ma première impression a été marquée par le son, le rythme du tam-tam. Et puis, les gens, le prêtre rituel, l’initié, toute une communauté, les objets, la danse, la botanique, l’eau et les plantes. C’est l’expression d’une vraie rencontre entre l’art et la science. C’était une bonne expérience. Ce culte animiste est une sorte d’humaniste, le fait de vivre ensemble est extrêmement important dans le Ndeup, qui implique le soin. On partage quelque chose ensemble du vivant. Le Ndeup est une manière de prendre soin du vivant. Un désir de vie commune.

**Giulia Paoletti** – Dans quel processus de relecture de ce matériel t’es-tu engagé ? Lors de cette résidence, tu as réalisé un projet d’installation, avec des archives en noir et blanc et des photographies contemporaines, pourquoi as-tu choisi ces images différentes?

**Ibrahima Thiam** – J’ai réalisé une première installation qui est composée de photographies du Ndeup en noir et blanc datant de 1950 qui sont associées à mes propres photographies. Je suis intéressé par le contemporain, et me demande comment ces photos prises dans les années 1950 raisonnent aujourd’hui. L’installation m’a permis de faire sortir au sein de l’espace public ces photos prisonnières des étagères et meubles du musée et de l’IFAN. J’ai utilisé le parc du musée comme un espace sociétal. Le parc est remis à l’art, c’est un environnement esthétique, pour les enfants, pour tout le monde.

**Giulia Paoletti** – Cette revue s’intitule « Trouble dans les collections » : comment ta démarche défie-t-elle les collections d’images ethnographiques produites par les colons ?

**Ibrahima Thiam** – Je pense que l’histoire et la mémoire nourrissent notre créativité. Les collections ethnographiques produites par les colons font partie de notre patrimoine. Il faut activer la pluralité de ces archives pour produire du sens, refonder du récit et la réinvention de soi. Il s’agit aussi de récupérer notre mémoire pour réinventer. Je pense vraiment qu’il est important de considérer la pluralité comme un élément essentiel de l’organisation des sociétés humaines.

![](img/04/04.jpg)
###### Auteur anonyme. Photographie datant de 1956, Mbott, plateau de Dakar, collection de l’IFAN Cheikh Anta Diop. 17 C 46 1516. Un des fromagers constituant l’ensemble de M’Both, angle rues Raffenel-Sandiniéry.

**Giulia Paoletti** – Dans le passé, tu as beaucoup travaillé sur les archives familiales, était-ce très diffèrent de travailler avec ces archives institutionnelles ?

**Ibrahima Thiam** – En 2009, quand j’ai commencé à faire de la photographie, j’ai fait des recherches sur l’histoire de la photographie africaine, j’ai compris que les photographies de ma collection en faisaient partie intégrante. J’ai entamé une collecte d’images à partir des archives de ma famille, puis j’ai commencé à chiner d’autres images. Les archives familiales et les archives de l’institution jouent le même rôle, elles nous permettent de revisiter nos histoires, individuelles et communes. Cela nous permet de posséder une solide compréhension du passé, une volonté d’explorer le présent et d’anticiper l’avenir.

**Giulia Paoletti** – Quel est ton projet à la suite de cette résidence ?

**Ibrahima Thiam** – Je souhaite installer une collection permanente de photographies dans le parc du musée. Afin de créer un lieu de pensée attractif. Je voudrais aussi organiser un Ndeup autour du baobab, lieu de conservation et de conversation, pour prendre soin et reconnecter la science et l’art. Ces propositions pourraient œuvrer pour un musée vivant, une invitation, un prélude peut être, à une connaissance plus large, plus ample, plus universelle, à la découverte des expériences les plus significatives de la vie des humains, traduisant le quotidien ou l’insolite, la volonté tantôt de s’assumer tantôt de se dépasser, le besoin de se reconnaitre ou de s’affirmer, le besoin de se situer, le besoin aussi de donner de la cohérence à la vie, au monde, à l’univers.

**Giulia Paoletti** – Ce n’est donc pas la restitution des objets appartenant à des collections occidentales qui retient ton intérêt, mais la reconnexion aux patrimoines multiples et vivants que vous avez au Sénégal ?

**Ibrahima Thiam** – Oui, une reconnexion pour redonner aux images une biographie qui dit leur errance et leurs transformations. Il nous faut explorer d’autres imaginaires, il ne s’agit pas strictement de faire des recherches historiques et d’écrire l’histoire, mais de nous inspirer de ces archives, de ces images du passé. Et d’interroger les partis pris scientifiques et visuels en mettant en scène de nouveaux récits.