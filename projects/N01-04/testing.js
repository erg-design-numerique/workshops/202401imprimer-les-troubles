class Pouces extends Paged.Handler {


    constructor(chunker, polisher, caller) {
      
      super(chunker, polisher, caller);
      this.chunker = chunker;
      
    }

    afterParsed(parsed){
        console.log(this.chunker.pageTemplate);
        this.changeTemplate(this.chunker.pageTemplate);
       
        
    }

    changeTemplate(template){
        const $contentZone = template.content.querySelector('.pagedjs_page_content');
        
        const $pouce = document.createElement('span');
        $pouce.classList.add('pouce');
        $contentZone.append($pouce);


    }
    beforePageLayout(page){
        
    }
    /*
    afterPageLayout(pageFragment, page) {
      console.log(pageFragment);
    }*/
  }
  Paged.registerHandlers(Pouces);