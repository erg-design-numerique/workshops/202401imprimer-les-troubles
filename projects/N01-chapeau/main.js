fetch('../../data/N01/json/numero_1.json')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        appendData(data);
    })
    .catch(function (err) {
        console.log(err);
    });


function appendData(data) {
    const $main = document.querySelector('main');
    // console.log(data);


    data.forEach(objet => {
        // pour chaque objet de la liste: afficher chacune de ses propriétés dans une div → boucler dedans

        const article = document.createElement("section");
        article.classList.add("article");
        $main.appendChild(article);
        const titre = document.createElement("h1");
        const soustitre = document.createElement("h2");
        const author = document.createElement("h3");
        const authorDescription = document.createElement("p");
    
        titre.innerHTML = objet.title;
        author.innerHTML = objet.author;
        soustitre.innerHTML = objet.subtitle;
        authorDescription.innerHTML = objet.authorDescription;
        article.appendChild(titre);
        article.appendChild(soustitre);
        article.appendChild(author);
        article.appendChild(authorDescription);

        // article.appendChild(authorDescription);


        //    console.log(typeof objet )
        // for (let e in objet) {
        //     console.log(objet[e])
        // }

    });

}

