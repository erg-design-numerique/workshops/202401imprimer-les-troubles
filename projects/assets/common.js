import microtypo from "./Microtypo.js";

let $padContent;
function insertData(data) {
    console.log(data);

   
    

    let converter = new showdown.Converter();

    let html = converter.makeHtml(data);
    html = microtypo.process(html);

    $padContent.append(html);
}

window.addEventListener('DOMContentLoaded', function(){

    let pageWidth = document.body.getAttribute('data-page-width');
    let pageHeight = document.body.getAttribute('data-page-height');
    console.log('dimensions de la page:', pageWidth, pageHeight);

    //on va ajouter une balise style dans le head de l'html contenant les dimensions de la page
    let style = document.createElement('style');
    style.textContent = `
    @page {
        size: ${pageWidth}mm ${pageHeight}mm;
        }       
    `;
    document.querySelector('head').appendChild(style);
    
    
    if(pageWidth == 210){
        document.querySelector('html').classList.add('a4');
    }else if(pageWidth == 105){
        document.querySelector('html').classList.add('a6');
    }else if(pageWidth == 297){
        document.querySelector('html').classList.add('a3');
    }

    $padContent = document.getElementById('pad-content');
    let request = new URL($padContent.getAttribute('data-md'));
    fetch(request).then(response => response.text()).then(data => {
        console.log(data);
        insertData(data);
        //entourer les imgs avec des <div>

        /*
        Hyphenopoly.config({
            
            require: {
                "fr": "FORCEHYPHENOPOLY"
            },
            setup: {
                selectors: {
                    "div": {"hyphen": "•"}
                }
            }
        });

        */
        window.PagedPolyfill.preview();
		document.querySelectorAll('.pagedjs_page_content').forEach((page) => {
			
			console.log('essai de wedge');	
			let bidule = document.createElement('div');
			bidule.classList.add('fingerwedge');
			page.appendChild(bidule);
		});
    });

/*
	let pages = document.querySelectorAll('.pagedjs_pagebox');
	let pagecount = pages.length;
	console.log("Pages:",pagecount);
	let pgindex = 1;
	pages.forEach( (page) => {
		
		page.style.background = `background: linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(34,34,134,1) ${(pgindex/pagecount)*100}%, rgba(255,255,255,1) 100%)`;
	});    
*/


});
