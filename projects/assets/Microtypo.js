const Microtypo = (({} = {}) => {
    
    const textNodesUnder = (el)  => {
        const children = [] // Type: Node[]
        const walker = document.createTreeWalker(el, NodeFilter.SHOW_TEXT)
        while(walker.nextNode()) {
          children.push(walker.currentNode)
        }
        return children
      }



    //insert fine non-breaking space before double punct signs (?!;:)
    const nbSpace = (root) => {
        //console.log(root.children);
        [...root.children].forEach(node => {
            const textNodes = textNodesUnder(node);
            textNodes.forEach(textNode => {
                textNode.textContent = textNode.textContent.replace(/\s?([;?:!»])\s?/, "\u202F$1 ");
                textNode.textContent = textNode.textContent.replace(/\s?([«])\s?/, " $1\u202F");
            });
        });
        //console.log(root.nodeType);
        
    };
    

    const process = (input) => {
        const frag = document.createRange().createContextualFragment(input);
        nbSpace(frag);

        return frag;
        
    };

    return {process};
})();

export default Microtypo;